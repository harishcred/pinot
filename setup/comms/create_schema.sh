curl --location --request POST 'http://localhost:9000/schemas?override=true' \
--header 'accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
    "schemaName": "comms_full_upsert_v1",
    "dimensionFieldSpecs": [
        {
            "name": "commsEventType",
            "dataType": "STRING"
        },
        {
            "name": "dim__channel",
            "dataType": "STRING"
        },
        {
            "name": "dim__pipeline",
            "dataType": "STRING"
        },
        {
            "name": "dim__provider_number",
            "dataType": "STRING"
        },
        {
            "name": "entityId",
            "dataType": "STRING"
        },
        {
            "name": "entityType",
            "dataType": "STRING"
        },
        {
            "name": "eventType",
            "dataType": "STRING"
        },
        {
            "name": "id",
            "dataType": "STRING"
        },
        {
            "name": "idType",
            "dataType": "STRING"
        },
        {
            "name": "provider",
            "dataType": "STRING"
        },
        {
            "name": "service",
            "dataType": "STRING"
        },
        {
            "name": "status",
            "dataType": "STRING"
        },
        {
            "name": "traceId",
            "dataType": "STRING"
        },
        {
            "name": "scheduled_timestamp",
            "dataType": "STRING"
        },
        {
            "name": "user_reply_timestamp",
            "dataType": "STRING"
        },
        {
            "name": "delivered_timestamp",
            "dataType": "STRING"
        },
        {
            "name": "sent_timestamp",
            "dataType": "STRING"
        },
        {
            "name": "userread_timestamp",
            "dataType": "STRING"
        }
    ],
    "dateTimeFieldSpecs": [
        {
            "name": "eventTime",
            "dataType": "TIMESTAMP",
            "format": "1:MILLISECONDS:EPOCH",
            "granularity": "1:MILLISECONDS"
        },
        {
            "name": "timestamp",
            "dataType": "STRING",
            "format": "1:HOURS:SIMPLE_DATE_FORMAT:yyyy-MM-dd'\''T'\''HH:mm:ss.SSS",
            "granularity": "1:MILLISECONDS"
        }
    ],
    "primaryKeyColumns": [
        "id",
        "idType"
    ]
}'