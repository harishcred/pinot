## Setup

1. Create a key from "EC2" -> "Key Pair". This key can be used to ssh into worker nodes

```shell
Generate public key from .pem key file
chmod 600 pinot-cluster.pem
ssh-keygen -y -f pinot-cluster.pem > /Users/harishbohara/keys/prod/id_rsa.pub
```

2. Update the subnet in ```cluster-prod.yaml``` file

3. Create cluster with ```eksctl```

```shell
eksctl create cluster -f cluster-prod.yaml

# Delete cluster if required
eksctl delete cluster -f cluster-prod.yaml
```

4. Access to AWS resources - create new roles and attach it to node-group role

```yaml
Create role - ds-platfrom-eks-parameter-store-read
  {
"Version": "2012-10-17",
"Statement": [
  {
    "Sid": "VisualEditor0",
    "Effect": "Allow",
    "Action": [
      "ssm:GetParametersByPath",
      "ssm:GetParameters",
      "ssm:GetParameter"
    ],
    "Resource": "arn:aws:ssm:*:280690977678:parameter/conf/ds-platform/stage/*"
  }
]
}

  Create role - s3ReadWrite-dp-batchplatform
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "VisualEditor0",
        "Effect": "Allow",
        "Action": "s3:ListBucket",
        "Resource": "arn:aws:s3:::dp-batchplatform-*"
      },
      {
        "Sid": "VisualEditor1",
        "Effect": "Allow",
        "Action": "s3:*",
        "Resource": [
          "arn:aws:s3:::dp-batchplatform-*/*",
          "arn:aws:s3:::dp-batchplatform-*"
        ]
      },
      {
        "Sid": "VisualEditor3",
        "Effect": "Allow",
        "Action": [
          "s3:GetO*",
          "s3:List*"
        ],
        "Resource": [
          "arn:aws:s3:::heracles-lending-stage/*",
          "arn:aws:s3:::heracles-lending-stage"
        ]
      }
    ]
  }
```

#### Ref docs

1. Doc to create - https://eksctl.io/usage/creating-and-managing-clusters/
