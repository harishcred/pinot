---
# Source: presto/templates/coordinator/configmap.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: v1
kind: ConfigMap
metadata:
  name: presto-catalog
data:
  pinot.properties: |
    connector.name=pinot
    pinot.controller-urls=pinot-controller:9000
    pinot.controller-rest-service=pinot-controller:9000
    pinot.allow-multiple-aggregations=true
    pinot.use-date-trunc=true
    pinot.infer-date-type-in-schema=true
    pinot.infer-timestamp-type-in-schema=true
    pinot.use-streaming-for-segment-queries=false
---
# Source: presto/templates/coordinator/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: presto-coordinator-etc
data:
  config.properties: |-
    coordinator=true
    node-scheduler.include-coordinator=true
    http-server.http.port=8080
    query.max-memory=4GB
    query.max-memory-per-node=1GB
    discovery-server.enabled=true
    discovery.uri=http://presto-coordinator:8080

  jvm.config:
    |-
      -server
      -Xmx16G
      -XX:+UseG1GC
      -XX:G1HeapRegionSize=32M
      -XX:+UseGCOverheadLimit
      -XX:+ExplicitGCInvokesConcurrent
      -XX:+HeapDumpOnOutOfMemoryError
      -XX:+ExitOnOutOfMemoryError

  log.properties:
    com.facebook.presto=INFO

  node.properties: |-
    node.environment=production
    node.data-dir=/home/presto/data
---
# Source: presto/templates/worker/configmap.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: v1
kind: ConfigMap
metadata:
  name: presto-worker-etc
data:
  config.properties: |-
    coordinator=false
    http-server.http.port=8080
    query.max-memory=8GB
    query.max-memory-per-node=4GB
    query.max-total-memory-per-node=8GB
    discovery.uri=http://presto-coordinator:8080

  jvm.config:
    |-
      -server
      -Xmx64G
      -XX:+UseG1GC
      -XX:G1HeapRegionSize=32M
      -XX:+UseGCOverheadLimit
      -XX:+ExplicitGCInvokesConcurrent
      -XX:+HeapDumpOnOutOfMemoryError
      -XX:+ExitOnOutOfMemoryError

  log.properties:
    com.facebook.presto=INFO

  node.properties: |-
    node.environment=production
    node.data-dir=/home/presto/data
---
# Source: presto/templates/coordinator/service-external.yaml
apiVersion: v1
kind: Service
metadata:
  name: presto-coordinator-external
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-internal: "true"
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: coordinator
    release: RELEASE-NAME
    heritage: Helm
spec:
  type: LoadBalancer
  ports:
    - name: external-coordinator
      port: 8080
  selector:
    app: presto
    release: RELEASE-NAME
    component: coordinator
---
# Source: presto/templates/coordinator/service-headless.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: v1
kind: Service
metadata:
  name: presto-coordinator-headless
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: coordinator
    release: RELEASE-NAME
    heritage: Helm
spec:
  clusterIP: None
  ports:
    # [pod_name].[service_name].[namespace].svc.cluster.local
    - port: 8080
  selector:
    app: presto
    release: RELEASE-NAME
    component: coordinator
---
# Source: presto/templates/coordinator/service.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: v1
kind: Service
metadata:
  name: presto-coordinator
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: coordinator
    release: RELEASE-NAME
    heritage: Helm
spec:
  type: ClusterIP
  ports:
    # [pod_name].[service_name].[namespace].svc.cluster.local
    - port: 8080
  selector:
    app: presto
    release: RELEASE-NAME
    component: coordinator
---
# Source: presto/templates/worker/service-headless.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: v1
kind: Service
metadata:
  name: presto-worker-headless
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: worker
    release: RELEASE-NAME
    heritage: Helm
spec:
  clusterIP: None
  ports:
    # [pod_name].[service_name].[namespace].svc.cluster.local
    - port: 8080
  selector:
    app: presto
    release: RELEASE-NAME
    component: worker
---
# Source: presto/templates/worker/service.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: v1
kind: Service
metadata:
  name: presto-worker
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: worker
    release: RELEASE-NAME
    heritage: Helm
spec:
  type: ClusterIP
  ports:
    # [pod_name].[service_name].[namespace].svc.cluster.local
    - port: 8080
  selector:
    app: presto
    release: RELEASE-NAME
    component: worker
---
# Source: presto/templates/coordinator/statefulset.yml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: presto-coordinator
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: coordinator
    release: RELEASE-NAME
    heritage: Helm
spec:
  selector:
    matchLabels:
      app: presto
      release: RELEASE-NAME
      component: coordinator
  serviceName: presto-coordinator-headless
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  podManagementPolicy: Parallel
  template:
    metadata:
      labels:
        app: presto
        release: RELEASE-NAME
        component: coordinator
      annotations:
        {}
    spec:
      nodeSelector:
        role: presto
      affinity:
        {}
      tolerations:
        - key: presto
          operator: Equal
          value: "1"
      securityContext:
        runAsGroup: 1000
        fsGroup: 1000
        runAsUser: 1000
      containers:
      - name: coordinator
        image: "apachepinot/pinot-presto:latest"
        imagePullPolicy: IfNotPresent
        args: [ "run" ]
        env:
        envFrom:
          [] 
        ports:
          - containerPort: 8080
            protocol: TCP
        volumeMounts:
          - name: presto-data
            mountPath: /home/presto/data
          - name: presto-catalog
            mountPath: "/home/presto/etc/catalog"
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/config.properties"
            subPath: config.properties
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/log.properties"
            subPath: log.properties
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/node.properties"
            subPath: node.properties
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/jvm.config"
            subPath: jvm.config
            readOnly: true
        resources:
            {}
      restartPolicy: Always
      volumes:
        - name: presto-catalog
          configMap:
            name: presto-catalog
        - name: presto-etc
          configMap:
            name: presto-coordinator-etc
  volumeClaimTemplates:
    - metadata:
        name: presto-data
        annotations:
          pv.beta.kubernetes.io/gid: "1000"
          pv.beta.kubernetes.io/groups: "1000"
      spec:
        accessModes:
          - "ReadWriteOnce"
        resources:
          requests:
            storage: 4G
---
# Source: presto/templates/worker/statefulset.yml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: presto-worker
  labels:
    app: presto
    chart: presto-0.2.2-SNAPSHOT
    component: worker
    release: RELEASE-NAME
    heritage: Helm
spec:
  selector:
    matchLabels:
      app: presto
      release: RELEASE-NAME
      component: worker
  serviceName: presto-worker-headless
  replicas: 1
  updateStrategy:
    type: RollingUpdate
  podManagementPolicy: Parallel
  template:
    metadata:
      labels:
        app: presto
        release: RELEASE-NAME
        component: worker
      annotations:
        {}
    spec:
      nodeSelector:
        role: presto
      affinity:
        {}
      tolerations:
        - key: presto
          operator: Equal
          value: "1"
      securityContext:
        runAsGroup: 1000
        fsGroup: 1000
        runAsUser: 1000
      containers:
      - name: coordinator
        image: "apachepinot/pinot-presto:latest"
        imagePullPolicy: IfNotPresent
        args: [ "run" ]
        env:
        envFrom:
          [] 
        ports:
          - containerPort: 8080
            protocol: TCP
        volumeMounts:
          - name: presto-data
            mountPath: /home/presto/data
          - name: presto-catalog
            mountPath: "/home/presto/etc/catalog"
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/config.properties"
            subPath: config.properties
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/log.properties"
            subPath: log.properties
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/node.properties"
            subPath: node.properties
            readOnly: true
          - name: presto-etc
            mountPath: "/home/presto/etc/jvm.config"
            subPath: jvm.config
            readOnly: true
        resources:
            {}
      restartPolicy: Always
      volumes:
        - name: presto-catalog
          configMap:
            name: presto-catalog
        - name: presto-etc
          configMap:
            name: presto-worker-etc
  volumeClaimTemplates:
    - metadata:
        name: presto-data
        annotations:
          pv.beta.kubernetes.io/gid: "1000"
          pv.beta.kubernetes.io/groups: "1000"
      spec:
        accessModes:
          - "ReadWriteOnce"
        resources:
          requests:
            storage: 10G
---
# Source: presto/templates/coordinator/service-external.yaml
#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
