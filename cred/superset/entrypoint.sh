#!/bin/sh

echo "====== Run SSM script ======"
pip install aws-ssm-devlibx==0.0.6

cat > /tmp/internal_scripts/ssm.py << EOL
from aws_ssm import ssm
print("Run SSM python script...")
ssm = ssm.SSM()
ssm.setup_env_from_ssm()
EOL
python /tmp/internal_scripts/ssm.py

echo "====== Print SSM ======"
cat /tmp/aws_ssm_env_devlibx

echo "====== Apply SSM ======"
. /tmp/aws_ssm_env_devlibx

echo "====== Print all env ======"
printenv

echo "====== All done ======"
exec "$@"
