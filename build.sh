
if [ $1 == "stage" ]; then

  helm template ./kubernetes/helm/pinot -n pinot-stage --set cluster.name=pinot-stage \
                      -f ./kubernetes/helm/pinot/values.yaml -f ./cred/stage/values.yaml > staging.yaml

  sed -i "" 's/RELEASE-NAME-//g' staging.yaml
  sed -i "" 's/pinot:latest-jdk11/pinot:0.10.0-SNAPSHOT-df1c2681fd-20220207-jdk11/g' staging.yaml
  sed -i "" 's|__S3_CONTROLLER_DIR__|s3://dp-batchplatform-stage/ds/pinot|g' staging.yaml
  sed -i "" 's|__S3_SERVER_DIR__|s3://dp-batchplatform-stage/ds/pinot|g' staging.yaml

  cp ./cred/endpoint_setup.yaml ./cred/stage/endpoint_setup.yaml
  sed -i "" 's|__NAMESPACE__|pinot-stage|g' ./cred/stage/endpoint_setup.yaml
  sed -i "" 's|__ENV__|stg|g' ./cred/stage/endpoint_setup.yaml

  # Setup presto
  helm template ./kubernetes/helm/presto -n pinot --values ./kubernetes/helm/presto/values.yaml > presto-staging.yaml
  sed -i "" "/query.max-total-memory-per-node=2GB/d" presto-staging.yaml
  sed -i "" 's/RELEASE-NAME-//g' presto-staging.yaml

  # Setup Superset
  cp ./kubernetes/helm/superset-shm.yaml ./cred/stage/superset-shm.yaml
  sed -i "" 's|__SUPERSET_SERVICE_NAME__|ds-platform|g' ./cred/stage/superset-shm.yaml
  sed -i "" 's|__ENV__|stage|g' ./cred/stage/superset-shm.yaml

elif [ $1 == "prod" ]; then

  helm template ./kubernetes/helm/pinot -n pinot --set cluster.name=pinot \
                    -f ./kubernetes/helm/pinot/values.yaml -f ./cred/prod/values.yaml > prod.yaml

  sed -i "" 's/RELEASE-NAME-//g' prod.yaml
  # sed -i "" 's/pinot:latest-jdk11/pinot:0.10.0-SNAPSHOT-df1c2681fd-20220207-jdk11/g' prod.yaml
  sed -i "" 's|__S3_CONTROLLER_DIR__|s3://data-platform-pinot/pinot/deepstorage-pinot|g' prod.yaml
  sed -i "" 's|__S3_SERVER_DIR__|s3://data-platform-pinot/pinot/deepstorage-pinot|g' prod.yaml

  # Setup presto
  helm template ./kubernetes/helm/presto -n pinot --values ./kubernetes/helm/presto/values.yaml > presto-prod.yaml
  sed -i "" "/query.max-total-memory-per-node=2GB/d" presto-prod.yaml
  sed -i "" 's/RELEASE-NAME-//g' presto-prod.yaml

  cp ./cred/endpoint_setup.yaml ./cred/prod/endpoint_setup.yaml
  sed -i "" 's|__NAMESPACE__|pinot|g' ./cred/prod/endpoint_setup.yaml
  sed -i "" 's|__ENV__|prod|g' ./cred/prod/endpoint_setup.yaml

  # Setup Superset
  cp ./kubernetes/helm/superset-shm.yaml ./cred/prod/superset-shm.yaml
  sed -i "" 's|__SUPERSET_SERVICE_NAME__|metric-store-superset|g' ./cred/prod/superset-shm.yaml
  sed -i "" 's|__ENV__|prod|g' ./cred/prod/superset-shm.yaml

elif [ $1 == "pre-prod" ]; then
  helm template ./kubernetes/helm/pinot -n pinot-backup --set cluster.name=backup-backup \
                      -f ./kubernetes/helm/pinot/values.yaml -f ./cred/prod/values-backup.yaml > prod-backup.yaml

  sed -i "" 's/RELEASE-NAME-//g' prod-backup.yaml
  # sed -i "" 's/pinot:latest-jdk11/pinot:0.10.0-SNAPSHOT-df1c2681fd-20220207-jdk11/g' prod-backup.yaml
  sed -i "" 's|__S3_CONTROLLER_DIR__|s3://data-platform-pinot/pinot/deepstorage-pinot-backup|g' prod-backup.yaml
  sed -i "" 's|__S3_SERVER_DIR__|s3://data-platform-pinot/pinot/deepstorage-pinot-backup|g' prod-backup.yaml
else
  echo "only dev/stage supported"
fi