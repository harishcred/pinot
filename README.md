
# How to deploy Pinot on dev/stage/production

NOTE - GO TO "prod-setup" branch

Assumption - you can create a K8S cluster with Istio form following sections.

```shell
1. sh build.sh <stage | prod>
  This will generate env specific file to deploy pinot
  
  For Stage:
  it will be "staging.yaml" for namespace=pinot-stage
  
  For Prod:
  it will be "prod.yaml" for namespace=pinot
  
kubectl apply -f staging.yaml -n pinot-stage 
   OR
kubectl apply -f prod.yaml -n pinot    

# Verify everything is fine
Pinot UI - http://localhost:9000
kubectl -n <pinot-stage | pinot> port-forward svc/pinot-controller-external 9000:9000
 >> You should be able to see Pinot Dashboad
```

---

# Full setup from start (Skip if you have a working Istio cluster) - Setup K8S and Istio

```shell
curl -L https://istio.io/downloadIstio | sh -
cd istio-1.12.2
export PATH=$PWD/bin:$PATH
# Copy value.yaml from <project>/cred/value.yaml
istioctl install --set profile=default -f value.yaml -y
kubectl label namespace default istio-injection=enabled
istioctl analyze

kubectl create ns <pinot-stage | pinot>
kubectl label namespace <pinot-stage | pinot> istio-injection=enabled

# Setup other tools
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/prometheus.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/kiali.yaml

# Get ELB
kubectl get svc -n istio-system

# Wait for everything to start "kubectl get all" -> it will take 1-2 min

# To create a endpoint which tou can access from stage
cd cred
kubectl apply -f kiali.yaml
>> you should be able to access dashboard at "http://kiali.dataplatform.stg.dreamplug.net/"

FOR PROD -> You should set a CNAME. You will get the ELB using "Get ELB" command given above.
Please do the set given in "Setup ALB endpoints for access Pinot and Superset" section for this
```

---

# Setup Superset

```shell
# Setup Superset    
 kubectl apply -f superset-shm.yaml -n <pinot-stage | pinot>   
# NOTE - to use SSM keys for Superset use "kubectl apply -f superset-shm.yaml" 

# Make sure things are fine -> wait for 2 min before you run this (PVC should be set-up)
kubectl get pvc -n  <pinot-stage | pinot>
kubectl describe pvc superset-storage-vol-superset-0 -n  <pinot-stage | pinot>
>> make sure the type is "gp2" not default

# Once it is running you will have to do this as a one time setup
# Super set user password:
kubectl exec -it pod/superset-0 -n <pinot-stage | pinot> -- bash -c 'flask fab create-admin'
kubectl exec -it pod/superset-0 -n <pinot-stage | pinot> -- bash -c 'superset db upgrade'
kubectl exec -it pod/superset-0 -n <pinot-stage | pinot> -- bash -c 'superset init'

# If it does not work then try following
Go to MySQL and create db "create database superset;"
kubectl exec -it pod/superset-0 -n <pinot-stage | pinot> -- bash
export SQLALCHEMY_DATABASE_URI=mysql://<user>:<password>@<url>/superset
flask fab create-admin
superset db upgrade
superset init

Superset UI - http://localhost:8088
kubectl -n pinot port-forward svc/superset 8088:8088

# Print Superset logs
kubectl -n pinot logs --follow pod/superset-0
```

#### Use MySQL as Superset backend

```shell
The default Superset uses sqlite. You can change it to MySQL, but the base image does not have support for MySQL. 
I have created a custom image for using MySQL

1. Update superset.yaml to use your new image with MySQL support:
Change "apachepinot/pinot-superset:latest" -> "docker.io/harishb2k/pinot-superset-ssm:0.0.5"

3. Update superset.yaml to use MySQL
Comment old SQLALCHEMY_DATABASE_URI and use following
SQLALCHEMY_DATABASE_URI = 'mysql://<user>:<password>@<host>/<dbname>'

4. Make sure your node group (not EKS but node group for EKS instances) has following permissions

Example - https://us-east-1.console.aws.amazon.com/iamv2/home#/roles/details/eks-nodegroup-role?section=permissions

NOTE - this config assumes your service name = "ds-platform" a env="stage"
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ssm:GetParametersByPath",
                "ssm:GetParameters",
                "ssm:GetParameter"
            ],
            "Resource": "arn:aws:ssm:*:280690977678:parameter/conf/ds-platform/stage/*"
        }
    ]
}
```

---

# Other useful set-up help

### Restart all Pinot service

```shell
STAGE:
  kubectl rollout restart -n pinot-stage statefulset.apps/pinot-broker statefulset.apps/pinot-controller statefulset.apps/pinot-minion statefulset.apps/pinot-server 
  kubectl rollout restart -n pinot-stage statefulset.apps/zookeeper
  kubectl rollout restart -n pinot-stage statefulset.apps/superset
  
PROD:  
  kubectl rollout restart -n pinot statefulset.apps/pinot-broker statefulset.apps/pinot-controller statefulset.apps/pinot-minion statefulset.apps/pinot-server 
  kubectl rollout restart -n pinot statefulset.apps/zookeeper
  kubectl rollout restart -n pinot statefulset.apps/superset  
```

---

### Setup ALB endpoints for access Pinot and Superset

```shell

# Go to Route53 and make the following 3 CNAMES:
# They all can point to same ALB which is created in Istio setup

pinot.dataplatform.<prod | stg>.dreamplug.net
superset.dataplatform.<prod | stg>.dreamplug.net

# Get the ALB info using following command - you can find the ALB info from the AWS EC2 dashbaord

# Apply "cred/endpoint_setup_stage.yaml" to enable these endpoints
kubectl apply -f cred/<prod | stage>/endpoint_setup_stage.yaml

# You should be able to access Pinot and Superset now
http://pinot.dataplatform.<prod | stg>.dreamplug.net
http://superset.dataplatform.<prod | stg>.dreamplug.net

```

### Setup and launch Dashboard (Not Mandatory)

```shell

# One time activity
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.5/aio/deploy/recommended.yaml
kubectl apply -f cred/eks-admin-service-account.yaml

# Get token to login
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
>> ca.crt:     1066 bytes
>> namespace:  11 bytes
>> token:      <SOME TOEKN TO BE USED>

# Run proxy and launch Dashboard
kubectl proxy
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/overview?namespace=default

```
